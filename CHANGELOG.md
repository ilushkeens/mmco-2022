# ММСО 2022 change log

## Version 1.0.0 under development

- New: Added all page templates
- New: Added Montserrat font
- New: Added jQuery, Swiper
- New: Added Pug, SCSS, PostCSS
- New: Added Webpack, Babel

## Project init February 23, 2022
