(function ($) {
  'use strict'

  // Плавно прокручивает к якорю
  $('.scroll-to').on('click', function (event) {
    event.preventDefault()

    let sectionId = $(this).attr('href'),
        pageTop = $(sectionId).offset().top

    closeMenu()

    $('html, body').animate({ scrollTop: pageTop }, 600)
  })

  // Показывает/скрывает меню при прокрутке (только для мобильных)
  if ($(window).width() < 1376) {
    let header = $('header'),
        scrollPrev = 0

    $(window).on('scroll', function () {
      let scrolled = $(window).scrollTop()

      if (scrolled > 90 && scrolled < scrollPrev) {
        header.addClass('out')
      } else {
        header.removeClass('out')
      }
      scrollPrev = scrolled
    })
  }

  // Функция открытия меню
  function openMenu() {
    $('html').css({ 'overflow-x': 'hidden', 'overflow-y': 'hidden' })
    $('#menu').show().animate({ opacity: 1 }, 300)
  }

  // Функция закрытия меню
  function closeMenu() {
    $('#menu').animate({ opacity: 0 }, 300, function () {
      history.pushState('', document.title, window.location.pathname)

      $('#menu').hide()
      $('html').css('overflow-y', 'visible')
    })
  }

  // Открывает/закрывает меню
  $('.open-menu').on('click', function (event) {
    event.preventDefault()

    let current = $(this),
        svg = current.find('svg use')

    if (current.hasClass('open')) {
      current.removeClass('open')
      svg.attr('href', '/image/icon/menu.svg#menu')
      closeMenu()
    }

    else {
      current.addClass('open')
      svg.attr('href', '/image/icon/close.svg#close')
      openMenu()

      event.stopPropagation()
    }
  })

  // Показывает подробную информацию
  $('.show-more').on('click', function (event) {
    event.preventDefault()

    let link = $(this).parents('.program-info__link')

    link.parent('.program-info').find('p').show()
    link.hide()
  })

  // Cтилизует select
  let currentSelect

  function openSelect(currentSelect) {
    $('.select-options[data-for="' + currentSelect + '"]').css('display', 'flex')
  }

  function closeSelect() {
    $('.select-options').hide()
  }

  // Показывает варианты при клике на поле
  $('.select-box').on('click', function (event) {
    event.preventDefault()

    currentSelect = $(this).attr('id')

    if ($(this).siblings('.select-options').is(':visible')) {
      closeSelect()
    } else {
      closeSelect()
      openSelect(currentSelect)

      event.stopPropagation()
    }
  })

  // Обрабатывает клик по вариантам
  $('.select-options').on('click', '.option', function () {
    $('#' + currentSelect).html($(this).text())
    $('input[name=' + currentSelect + ']').val($(this).attr('data-value'))

    closeSelect()
  })

  // Прячет варианты
  $(document).on('click', function (event) {
    if (!$('.option').is(event.target)) {
      closeSelect()
    }
  })

  // Обрабатывает отправку форм
  $('form').each(function () {
    let form = $(this),
        button = form.find('.button_send')

    // Проверяет заполнение обязательных полей формы
    function checkInput() {
      form.find('textarea, input').each(function () {
        let field = $(this)

        if (field.attr('required') !== undefined) {
          if (field.val() != '') {
            field.removeClass('empty')
          } else {
            field.addClass('empty')
          }
        }
      })
    }

    // Подсвечивает незаполненные поля
    function lightEmpty() {
      form.find('.empty').addClass('error')

      setTimeout(function () {
        form.find('.empty').removeClass('error')
      }, 900)
    }

    setInterval(function () {
      checkInput()

      let countEmpty = form.find('.empty').length

      if (countEmpty > 0) {
        if (button.hasClass('button_disabled')) {
          return false
        } else {
          button.addClass('button_disabled')
        }
      } else {
        button.removeClass('button_disabled')
      }
    }, 1000)

    // Обрабатывает попытку отправки формы
    button.on('click', 'a', function (event) {
      event.preventDefault()

      if (button.hasClass('button_disabled')) {
        // Подсвечивает незаполненные поля
        lightEmpty()
      } else {
        // Отправляет форму
        button.addClass('button_disabled')

        form.on('submit')
        form.find('textarea, input').val('')

        // Скрывает поля регистрации
        $('#registry, #registry-2').hide()
        $('#success, #success-2').show()
      }
    })
  })

  // Показывает дополнительную информацию об экспертах
  $('.open-info').on('click', function (event) {
    event.preventDefault()

    let current = $(this).closest('.experts-list__item')

    current.find('.experts-list-info').show()
  })

  $('.close-info').on('click', function (event) {
    event.preventDefault()

    let current = $(this).closest('.experts-list__item')

    current.find('.experts-list-info').hide()
  })

  // Добавляет раскрывающийся список
  $('.open-list').on('click', function (event) {
    event.preventDefault()

    let current = $(this).parent('.partners__item')

    if (current.hasClass('open')) {
      current.removeClass('open')
    } else {
      current.addClass('open')
    }
  })
}(jQuery))
