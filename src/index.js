'use strict'

// Swiper
import Swiper, { Navigation } from 'swiper';

// Добавляет слайдер с экспертами
const expertsSwiper = new Swiper('.experts-swiper', {
  modules: [Navigation],
  slidesPerView: 'auto',
  spaceBetween: 16,
  breakpoints: {
    1376: {
      slidesPerView: 3,
      spaceBetween: 32
    },
    1600: {
      slidesPerView: 4
    }
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev'
  }
});

// Добавляет слайдеры с продукцией и новостями
const newsSwiper = new Swiper('.news-swiper', {
  modules: [Navigation],
  slidesPerView: 1,
  spaceBetween: 16,
  breakpoints: {
    1376: {
      slidesPerView: 'auto',
      spaceBetween: 60
    }
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev'
  }
});

// js
import './js/main'

// css
import './assets/css/main.css'

// scss
import './assets/scss/main.scss'
